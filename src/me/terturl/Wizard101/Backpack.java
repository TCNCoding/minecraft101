package me.terturl.Wizard101;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Backpack implements Listener, CommandExecutor {
	
	public Main plugin;
	public static Inventory MainInventory;
	public static Inventory MainShop;

	public Inventory onDeathInventoryOpen(Player p) {
		ItemStack playerinfo = new ItemStack(Material.LEATHER_HELMET, 1);
		ItemMeta pi1 = playerinfo.getItemMeta();
		pi1.setDisplayName("§aPlayer Info");
		playerinfo.setItemMeta(pi1);
		
		ItemStack Inventory = new ItemStack(Material.CHEST, 1);
		ItemMeta i1 = Inventory.getItemMeta();
		i1.setDisplayName("§aInventory");
		Inventory.setItemMeta(i1);
		
		ItemStack Deck = new ItemStack(Material.ENCHANTED_BOOK, 1);
		ItemMeta d1 = Deck.getItemMeta();
		d1.setDisplayName("§aSpell Deck");
		Deck.setItemMeta(d1);
		
		ItemStack Quests = new ItemStack(Material.PAPER, 1);
		ItemMeta q1 = Quests.getItemMeta();
		q1.setDisplayName("§aQuests");
		Quests.setItemMeta(q1);
		
		ItemStack WorldMap = new ItemStack(Material.MAP, 1);
		ItemMeta wm1 = WorldMap.getItemMeta();
		wm1.setDisplayName("§aWorldMap");
		WorldMap.setItemMeta(wm1);
		
		ItemStack Credits = new ItemStack(Material.BOOK, 1);
		ItemMeta c1 = Credits.getItemMeta();
		c1.setDisplayName("§aCredits");
		Credits.setItemMeta(c1);
		
		ItemStack Realms = new ItemStack(Material.ENDER_CHEST, 1);
		ItemMeta r1 = Realms.getItemMeta();
		r1.setDisplayName("§aRealms");
		Realms.setItemMeta(r1);
		
		ItemStack Settings = new ItemStack(Material.NETHER_STAR, 1);
		ItemMeta s1 = Settings.getItemMeta();
		s1.setDisplayName("§aSettings");
		Settings.setItemMeta(s1);
		
		ItemStack Close = new ItemStack(Material.ANVIL, 1);
		ItemMeta cl1 = Close.getItemMeta();
		cl1.setDisplayName("§aStore");
		Close.setItemMeta(cl1);
		
		MainInventory = Bukkit.createInventory(null, 9, "§3§lBackpack");
		
		MainInventory.clear();
		MainInventory.setItem(0, playerinfo);
		MainInventory.setItem(1, Inventory);
		MainInventory.setItem(2, Deck);
		MainInventory.setItem(3, Quests);
		MainInventory.setItem(4, WorldMap);
		MainInventory.setItem(5, Credits);
		MainInventory.setItem(6, Realms);
		MainInventory.setItem(7, Settings);
		MainInventory.setItem(8, Close);
		return MainInventory;
	}
	
	public Inventory onMainShopOpen(Player p) {
		ItemStack Level1 = new ItemStack(Material.BEACON, 1);
		ItemMeta l1 = Level1.getItemMeta();
		l1.setDisplayName("§4Level 1");
		Level1.setItemMeta(l1);
		
		ItemStack Level5 = new ItemStack(Material.BEACON, 1);
		ItemMeta l2 = Level5.getItemMeta();
		l2.setDisplayName("§cLevel 5");
		Level5.setItemMeta(l2);
		
		ItemStack Level8 = new ItemStack(Material.BEACON, 1);
		ItemMeta l3 = Level8.getItemMeta();
		l3.setDisplayName("§6Level 8");
		Level8.setItemMeta(l3);
		
		ItemStack Level10 = new ItemStack(Material.BEACON, 1);
		ItemMeta l4 = Level10.getItemMeta();
		l4.setDisplayName("§eLevel 10");
		Level10.setItemMeta(l4);
		
		ItemStack Level15 = new ItemStack(Material.BEACON, 1);
		ItemMeta l5 = Level15.getItemMeta();
		l5.setDisplayName("§2Level 15");
		Level15.setItemMeta(l5);
		
		ItemStack Level18 = new ItemStack(Material.BEACON, 1);
		ItemMeta l6 = Level18.getItemMeta();
		l6.setDisplayName("§aLevel 18");
		Level18.setItemMeta(l6);
		
		ItemStack Level20 = new ItemStack(Material.BEACON, 1);
		ItemMeta l7 = Level20.getItemMeta();
		l7.setDisplayName("§bLevel 20");
		Level20.setItemMeta(l7);
		
		ItemStack Level25 = new ItemStack(Material.BEACON, 1);
		ItemMeta l8 = Level25.getItemMeta();
		l8.setDisplayName("§3Level 25");
		Level25.setItemMeta(l8);
		
		ItemStack Level28 = new ItemStack(Material.BEACON, 1);
		ItemMeta l9 = Level28.getItemMeta();
		l9.setDisplayName("§1Level 28");
		Level28.setItemMeta(l9);
		
		ItemStack Level30 = new ItemStack(Material.BEACON, 1);
		ItemMeta l10 = Level30.getItemMeta();
		l10.setDisplayName("§9Level 30");
		Level30.setItemMeta(l10);
		
		ItemStack Level35 = new ItemStack(Material.BEACON, 1);
		ItemMeta l11 = Level35.getItemMeta();
		l11.setDisplayName("§dLevel 35");
		Level35.setItemMeta(l11);
		
		ItemStack Level38 = new ItemStack(Material.BEACON, 1);
		ItemMeta l12 = Level38.getItemMeta();
		l12.setDisplayName("§5Level 38");
		Level38.setItemMeta(l12);
		
		ItemStack Level40 = new ItemStack(Material.BEACON, 1);
		ItemMeta l13 = Level40.getItemMeta();
		l13.setDisplayName("§fLevel 40");
		Level40.setItemMeta(l13);
		
		ItemStack Level45 = new ItemStack(Material.BEACON, 1);
		ItemMeta l14 = Level45.getItemMeta();
		l14.setDisplayName("§7Level 45");
		Level45.setItemMeta(l14);
		
		ItemStack Level48 = new ItemStack(Material.BEACON, 1);
		ItemMeta l15 = Level48.getItemMeta();
		l15.setDisplayName("§8Level 48");
		Level48.setItemMeta(l15);
		
		ItemStack Level50 = new ItemStack(Material.BEACON, 1);
		ItemMeta l16 = Level50.getItemMeta();
		l16.setDisplayName("§0Level 50");
		Level50.setItemMeta(l16);
		
		ItemStack Level52 = new ItemStack(Material.BEACON, 1);
		ItemMeta l17 = Level52.getItemMeta();
		l17.setDisplayName("§4Level 52");
		Level52.setItemMeta(l17);
		
		ItemStack Level54 = new ItemStack(Material.BEACON, 1);
		ItemMeta l18 = Level54.getItemMeta();
		l18.setDisplayName("§6Level 54");
		Level54.setItemMeta(l18);
		
		ItemStack Level56 = new ItemStack(Material.BEACON, 1);
		ItemMeta l19 = Level56.getItemMeta();
		l19.setDisplayName("§eLevel 56");
		Level56.setItemMeta(l19);
		
		ItemStack Level58 = new ItemStack(Material.BEACON, 1);
		ItemMeta l20 = Level58.getItemMeta();
		l20.setDisplayName("§aLevel 58");
		Level58.setItemMeta(l20);
		
		ItemStack Level60 = new ItemStack(Material.BEACON, 1);
		ItemMeta l21 = Level60.getItemMeta();
		l21.setDisplayName("§bLevel 60");
		Level60.setItemMeta(l21);
		
		ItemStack Level62 = new ItemStack(Material.BEACON, 1);
		ItemMeta l22 = Level62.getItemMeta();
		l22.setDisplayName("§3Level 62");
		Level62.setItemMeta(l22);
		
		ItemStack Level64 = new ItemStack(Material.BEACON, 1);
		ItemMeta l23 = Level64.getItemMeta();
		l23.setDisplayName("§1Level 64");
		Level64.setItemMeta(l23);
		
		ItemStack Level66 = new ItemStack(Material.BEACON, 1);
		ItemMeta l24 = Level66.getItemMeta();
		l24.setDisplayName("§9Level 66");
		Level66.setItemMeta(l24);
		
		ItemStack Level68 = new ItemStack(Material.BEACON, 1);
		ItemMeta l25 = Level68.getItemMeta();
		l25.setDisplayName("§dLevel 68");
		Level68.setItemMeta(l25);
		
		ItemStack Level70 = new ItemStack(Material.BEACON, 1);
		ItemMeta l26 = Level70.getItemMeta();
		l26.setDisplayName("§5Level 70");
		Level70.setItemMeta(l26);
		
		ItemStack Level72 = new ItemStack(Material.BEACON, 1);
		ItemMeta l27 = Level72.getItemMeta();
		l27.setDisplayName("§fLevel 72");
		Level72.setItemMeta(l27);
		
		ItemStack PlaceHolder = new ItemStack(Material.PISTON_EXTENSION, 1);
		ItemMeta PH = PlaceHolder.getItemMeta();
		PH.setDisplayName("§aSpace");
		PlaceHolder.setItemMeta(PH);
		
		ItemStack Level74 = new ItemStack(Material.BEACON, 1);
		ItemMeta l28 = Level74.getItemMeta();
		l28.setDisplayName("§7Level 74");
		Level74.setItemMeta(l28);
		
		ItemStack Level76 = new ItemStack(Material.BEACON, 1);
		ItemMeta l29 = Level76.getItemMeta();
		l29.setDisplayName("§8Level 76");
		Level76.setItemMeta(l29);
		
		ItemStack Level78 = new ItemStack(Material.BEACON, 1);
		ItemMeta l30 = Level78.getItemMeta();
		l30.setDisplayName("§0Level 78");
		Level78.setItemMeta(l30);
		
		ItemStack Level80 = new ItemStack(Material.BEACON, 1);
		ItemMeta l31 = Level80.getItemMeta();
		l31.setDisplayName("§4Level 80");
		Level80.setItemMeta(l31);
		
		ItemStack Level85 = new ItemStack(Material.BEACON, 1);
		ItemMeta l32 = Level85.getItemMeta();
		l32.setDisplayName("§eLevel 85");
		Level85.setItemMeta(l32);
		
		ItemStack Level90 = new ItemStack(Material.BEACON, 1);
		ItemMeta l33 = Level90.getItemMeta();
		l33.setDisplayName("§2Level 90");
		Level90.setItemMeta(l33);
		
		MainShop = Bukkit.createInventory(null, 36, "§b§lArmor and Items");

		MainShop.clear();
		MainShop.setItem(0, Level1);
		MainShop.setItem(1, Level5);
		MainShop.setItem(2, Level8);
		MainShop.setItem(3, Level10);
		MainShop.setItem(4, Level15);
		MainShop.setItem(5, Level18);
		MainShop.setItem(6, Level20);
		MainShop.setItem(7, Level25);
		MainShop.setItem(8, Level28);
		MainShop.setItem(9, Level30);
		MainShop.setItem(10, Level35);
		MainShop.setItem(11, Level38);
		MainShop.setItem(12, Level40);
		MainShop.setItem(13, Level45);
		MainShop.setItem(14, Level48);
		MainShop.setItem(15, Level50);
		MainShop.setItem(16, Level52);
		MainShop.setItem(17, Level54);
		MainShop.setItem(18, Level56);
		MainShop.setItem(19, Level58);
		MainShop.setItem(20, Level60);
		MainShop.setItem(21, Level62);
		MainShop.setItem(22, Level64);
		MainShop.setItem(23, Level66);
		MainShop.setItem(24, Level68);
		MainShop.setItem(25, Level70);
		MainShop.setItem(26, Level72);
		MainShop.setItem(27, PlaceHolder);
		MainShop.setItem(28, Level74);
		MainShop.setItem(29, Level76);
		MainShop.setItem(30, Level78);
		MainShop.setItem(31, PlaceHolder);
		MainShop.setItem(32, Level80);
		MainShop.setItem(33, Level85);
		MainShop.setItem(34, Level90);
		MainShop.setItem(35, PlaceHolder);
		
		return MainShop;
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
    public void onCompassInteractEvent(PlayerInteractEvent e) {
		Player player = e.getPlayer();
        Action rightClickAir = Action.RIGHT_CLICK_AIR;
        Action rightClickBlock = Action.RIGHT_CLICK_BLOCK;

        if(player.getItemInHand().getType()==Material.SUGAR_CANE_BLOCK){
            	if(e.getAction().equals(rightClickBlock)||e.getAction().equals(rightClickAir)){
            			player.openInventory(onDeathInventoryOpen(player));
            }
        }
	}
	
	@EventHandler
	 public void inventoryClick(InventoryClickEvent e) {
		 Player p = (Player)e.getWhoClicked();
		 
		 if(e.getInventory().equals(p.getInventory())) {
			 if(e.getCurrentItem() == null)
				 return;
			 if((e.getCurrentItem().getType() == Material.SUGAR_CANE_BLOCK) && e.getCurrentItem().getItemMeta().hasDisplayName()
					 && e.getCurrentItem().getItemMeta().getDisplayName().contains(ChatColor.translateAlternateColorCodes('&', "&6&lBackpack"))) {
				 e.setCancelled(true);
			 }
		 }
	 }
	
	@EventHandler
	 public void onClick(InventoryClickEvent e) {
		Player p = (Player)e.getWhoClicked();
		
	  if (e.getInventory().getName().equalsIgnoreCase(MainInventory.getName())) {
	   if (e.getCurrentItem() == null)
	    return;
	   if (e.getCurrentItem().getType() == Material.LEATHER_HELMET
	     && e.getCurrentItem().getItemMeta().hasDisplayName()
	     && e.getCurrentItem().getItemMeta().getDisplayName()
	       .contains("§aPlayer Info")) {
	    e.setCancelled(true);
	    e.getWhoClicked().closeInventory();
	    ((Player) e.getWhoClicked()).chat("/playerinfo");
	   }
	   if (e.getCurrentItem().getType() == Material.CHEST
	     && e.getCurrentItem().getItemMeta().hasDisplayName()
	     && e.getCurrentItem().getItemMeta().getDisplayName()
	       .contains("§aInventory")) {
	    e.setCancelled(true);
	    e.getWhoClicked().closeInventory();
	    p.openInventory(p.getInventory());
	   }
	   if (e.getCurrentItem().getType() == Material.ENCHANTED_BOOK
	     && e.getCurrentItem().getItemMeta().hasDisplayName()
	     && e.getCurrentItem().getItemMeta().getDisplayName()
	       .contains("§aSpell Deck")) {
	    e.setCancelled(true);
	    e.getWhoClicked().closeInventory();
	    ((Player) e.getWhoClicked()).chat("/spelldeck");
	   }
	   if (e.getCurrentItem().getType() == Material.PAPER
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§aQuests")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/quests");
			   }
	   if (e.getCurrentItem().getType() == Material.MAP
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§aWorldMap")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/map");
			   }
	   if (e.getCurrentItem().getType() == Material.BOOK
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§aCredits")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/credits");
			   }
	   if (e.getCurrentItem().getType() == Material.ENDER_CHEST
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§aRealms")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/pickrealm");
			   }
	   if (e.getCurrentItem().getType() == Material.NETHER_STAR
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§aSettings")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/settings");
			   }
	   if (e.getCurrentItem().getType() == Material.ANVIL
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§aStore")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/storegui");
			   }
	  		}
	}
	
	@EventHandler
	 public void onClick2(InventoryClickEvent e) {
	  if (e.getInventory().getName().equalsIgnoreCase(MainShop.getName())) {
	   if (e.getCurrentItem() == null)
	    return;
	   if (e.getCurrentItem().getType() == Material.BEACON
			   	 && e.getCurrentItem().getItemMeta().hasDisplayName()
	     		 && e.getCurrentItem().getItemMeta().getDisplayName()
	     			.contains("§4Level 1")) {
		   		e.setCancelled(true);
	    		e.getWhoClicked().closeInventory();
	    		((Player) e.getWhoClicked()).chat("/store1");
	   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§cLevel 5")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store5");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§6Level 8")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store8");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§eLevel 10")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store10");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§2Level 15")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store15");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§aLevel 18")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store18");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§bLevel 20")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store20");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§3Level 25")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store25");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§1Level 28")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store28");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§9Level 30")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store30");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§dLevel 35")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store35");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§5Level 38")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store38");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§fLevel 40")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store40");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§7Level 45")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store45");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§8Level 48")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store48");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§0Level 50")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store50");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§4Level 52")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store52");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§6Level 54")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store54");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§eLevel 56")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store56");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§aLevel 58")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store58");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§bLevel 60")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store60");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§3Level 62")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store62");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§1Level 64")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store64");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§9Level 66")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store66");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§dLevel 68")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store68");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§5Level 70")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store70");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§fLevel 72")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/store72");
			   }
	   if (e.getCurrentItem().getType() == Material.PISTON_EXTENSION
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§aSpace")) {
			    e.setCancelled(true);
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§7Level 74")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/level74");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§8Level 76")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/level76");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§0Level 78")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/level78");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§4Level 80")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/level80");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§eLevel 85")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/level85");
			   }
	   if (e.getCurrentItem().getType() == Material.BEACON
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .contains("§2Level 90")) {
			    e.setCancelled(true);
			    e.getWhoClicked().closeInventory();
			    ((Player) e.getWhoClicked()).chat("/level90");
			   }
	  }
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label,
			String[] args) {
		Player p = (Player)sender;
		
		if(label.equalsIgnoreCase("storegui")) {
			if(args.length == 0) {
				p.openInventory(onMainShopOpen(p));
			}
		}
		return false;
	}
}