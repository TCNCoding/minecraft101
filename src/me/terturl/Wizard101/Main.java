package me.terturl.Wizard101;

import java.util.logging.Level;
import java.util.logging.Logger;

import me.terturl.Wizard101.commands.CommandGiveArenaTickets;
import me.terturl.Wizard101.commands.CommandGiveCrowns;
import me.terturl.Wizard101.commands.CommandGiveGold;
import me.terturl.Wizard101.commands.Credits;
import me.terturl.Wizard101.commands.Map;
import me.terturl.Wizard101.commands.PlayerInfo;
import me.terturl.Wizard101.commands.SpellDeck;
import me.terturl.Wizard101.commands.store.Store1;
import me.terturl.Wizard101.commands.store.Store5;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	
	public ItemStack BootsOfTheComet = new ItemStack(Material.GOLD_BOOTS);
	public ItemStack BootsOfTheSlipstream = new ItemStack(Material.LEATHER_BOOTS);
	public ItemStack CardinalBoots = new ItemStack(Material.LEATHER_BOOTS);
	
	public static final String _prefix = ChatColor.AQUA + "[Wizard101] ";
	
	public void onEnable() {
		Bukkit.getPluginManager().registerEvents(new PlayerJoin(), this);
		Bukkit.getPluginManager().registerEvents(new Backpack(), this);
		Bukkit.getPluginManager().registerEvents(new Credits(), this);
		
		getCommand("storegui").setExecutor(new Backpack());
		getCommand("givec").setExecutor(new CommandGiveCrowns());
		getCommand("giveg").setExecutor(new CommandGiveGold());
		getCommand("giveat").setExecutor(new CommandGiveArenaTickets());
		getCommand("store1").setExecutor(new Store1());
		getCommand("store5").setExecutor(new Store5());
		getCommand("playerinfo").setExecutor(new PlayerInfo());
		getCommand("spelldeck").setExecutor(new SpellDeck());
		getCommand("credits").setExecutor(new Credits());
		getCommand("map").setExecutor(new Map());
	}
	
	public void onDisable() {
	}
	
	public static void sendConsoleMessage(String msg) {
        // My Nice Colored Console Message Prefix.
        Logger.getLogger("Minecraft").log(Level.INFO, _prefix + ChatColor.AQUA + msg);
    }
	
	public boolean onCommand(CommandSender cs, Command command, String label, String[] args) {
		
		Player p = (Player)cs;
		
		if(label.equalsIgnoreCase("setRavenWoodSpawn")) {
			if(p.isOp()) {
				getConfig().addDefault("Spawns.RavenWood.World", p.getLocation().getWorld());
				getConfig().addDefault("Spawns.RavenWood.X", Integer.valueOf(p.getLocation().getBlockX()));
				getConfig().addDefault("Spawns.RavenWood.Y", Integer.valueOf(p.getLocation().getBlockY()));
				getConfig().addDefault("Spawns.RavenWood.Z", Integer.valueOf(p.getLocation().getBlockZ()));
				getConfig().options().copyDefaults(true);
				saveConfig();
			}
		}
		
		if(label.equalsIgnoreCase("setUnicorn")) {
			if(p.isOp()) {
				getConfig().addDefault("Spawns.UnicornWay.World", p.getLocation().getWorld());
				getConfig().addDefault("Spawns.UnicornWay.X", Integer.valueOf(p.getLocation().getBlockX()));
				getConfig().addDefault("Spawns.UnicornWay.Y", Integer.valueOf(p.getLocation().getBlockY()));
				getConfig().addDefault("Spawns.UnicornWay.Z", Integer.valueOf(p.getLocation().getBlockZ()));
				getConfig().options().copyDefaults(true);
				saveConfig();
			}
		}
		
		if(label.equalsIgnoreCase("setRespawn")) {
			if(p.isOp()) {
				getConfig().addDefault("Spawns.Respawn.World", p.getLocation().getWorld());
				getConfig().addDefault("Spawns.Respawn.X", Integer.valueOf(p.getLocation().getBlockX()));
				getConfig().addDefault("Spawns.Respawn.Y", Integer.valueOf(p.getLocation().getBlockY()));
				getConfig().addDefault("Spawns.Respawn.Z", Integer.valueOf(p.getLocation().getBlockZ()));
				getConfig().options().copyDefaults(true);
				saveConfig();
			}
		}
		
		if(label.equalsIgnoreCase("setShoppingDistrict")) {
			if(p.isOp()) {
				getConfig().addDefault("Spawns.ShoppingD.World", p.getLocation().getWorld());
				getConfig().addDefault("Spawns.ShoppingD.X", Integer.valueOf(p.getLocation().getBlockX()));
				getConfig().addDefault("Spawns.ShoppingD.Y", Integer.valueOf(p.getLocation().getBlockY()));
				getConfig().addDefault("Spawns.ShoppingD.Z", Integer.valueOf(p.getLocation().getBlockZ()));
				getConfig().options().copyDefaults(true);
				saveConfig();
			}
		}
		
		if(label.equalsIgnoreCase("setGolemCourt")) {
			if(p.isOp()) {
				getConfig().addDefault("Spawns.GoldCourt.World", p.getLocation().getWorld());
				getConfig().addDefault("Spawns.GolemCourt.X", Integer.valueOf(p.getLocation().getBlockX()));
				getConfig().addDefault("Spawns.GolemCourt.Y", Integer.valueOf(p.getLocation().getBlockY()));
				getConfig().addDefault("Spawns.GolemCourt.Z", Integer.valueOf(p.getLocation().getBlockZ()));
				getConfig().options().copyDefaults(true);
				saveConfig();
			}
		}
		
		if(label.equalsIgnoreCase("setOldeTown")) {
			if(p.isOp()) {
				getConfig().addDefault("Spawns.OldeTown.World", p.getLocation().getWorld());
				getConfig().addDefault("Spawns.OldeTown.X", Integer.valueOf(p.getLocation().getBlockX()));
				getConfig().addDefault("Spawns.OldeTown.Y", Integer.valueOf(p.getLocation().getBlockY()));
				getConfig().addDefault("Spawns.OldeTown.Z", Integer.valueOf(p.getLocation().getBlockZ()));
				getConfig().options().copyDefaults(true);
				saveConfig();
			}
		}
		
		if(label.equalsIgnoreCase("setTritonAvenue")) {
			if(p.isOp()) {
				getConfig().addDefault("Spawns.TritonAvenue.World", p.getLocation().getWorld());
				getConfig().addDefault("Spawns.TritonAvenue.X", Integer.valueOf(p.getLocation().getBlockX()));
				getConfig().addDefault("Spawns.TritonAvenue.Y", Integer.valueOf(p.getLocation().getBlockY()));
				getConfig().addDefault("Spawns.TritonAvenue.Z", Integer.valueOf(p.getLocation().getBlockZ()));
				getConfig().options().copyDefaults(true);
				saveConfig();
			}
		}
		
		return false;
	}
}