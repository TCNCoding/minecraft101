package me.terturl.Wizard101.events;

import me.terturl.Wizard101.Main;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class SignChange implements Listener {
	
	public Main plugin;
	
	@EventHandler
	public void Sign(SignChangeEvent e) {
		Player p = e.getPlayer();
		if(e.getLine(0).equalsIgnoreCase("[spawnRavenWoord]")) {
			if(p.isOp()) {
				e.setLine(0, "To RavenWood");
				e.setLine(1, "============");
			}
		}
	}
	
	@EventHandler
	public void Interact(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Block b = e.getClickedBlock();
		
		if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if(b.getType() == Material.WALL_SIGN) {
				org.bukkit.block.Sign sign = (org.bukkit.block.Sign)b.getState();
				if(((sign.getLine(0).equalsIgnoreCase("To RavenWood")) && (sign.getLine(1).equalsIgnoreCase("============")))) {
					double x = plugin.getConfig().getDouble("Spawns.RavenWood.X");
					double y = plugin.getConfig().getDouble("Spawns.RavenWood.Y");
					double z = plugin.getConfig().getDouble("Spawns.RavenWood.Z");
					String world = plugin.getConfig().getString("Spawns.RavenWood.World");
					p.teleport(new Location(Bukkit.getWorld(world), x, y, z));
				}
			}
		}
	}
	
}