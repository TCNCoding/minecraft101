package me.terturl.Wizard101.items;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import me.terturl.Wizard101.Main;

public class AnyLevelArmorBoots {
	
	public Main plugin;
	
	public void registerBootsOfTheComet() {
		ItemMeta boots = plugin.BootsOfTheComet.getItemMeta();
		boots.setDisplayName(ChatColor.GOLD + "Boots Of The Comet");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.RED + "Speed: 8%");
		boots.setLore(lore);
		plugin.BootsOfTheComet.setItemMeta(boots);
	}
	
	public void registerBootsOfTheSlipstream() {
		ItemMeta boots = plugin.BootsOfTheSlipstream.getItemMeta();
		LeatherArmorMeta am = (LeatherArmorMeta)((ItemStack) boots).getItemMeta();
		am.setColor(Color.GREEN);
		boots.setDisplayName(ChatColor.GOLD + "Boots Of The Slipstream");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.RED + "Speed: 4%");
		boots.setLore(lore);
		plugin.BootsOfTheSlipstream.setItemMeta(boots);
	}
	
	public void registerCardinalBoots() {
		ItemMeta boots = plugin.CardinalBoots.getItemMeta();
		LeatherArmorMeta am = (LeatherArmorMeta)((ItemStack) boots).getItemMeta();
		am.setColor(Color.GREEN);
		boots.setDisplayName(ChatColor.GOLD + "Cardinal Boots");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.RED + "Health: + 10%");
		boots.setLore(lore);
		plugin.CardinalBoots.setItemMeta(boots);
	}
	
}