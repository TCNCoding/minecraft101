package me.terturl.Wizard101;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PlayerJoin implements Listener {
	
	@EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent e){

        Player p = e.getPlayer();
        ItemStack compass = new ItemStack(Material.SUGAR_CANE_BLOCK, 1);

        Inventory inv = p.getInventory();
        
        if(!p.hasPlayedBefore()) {
        	p.sendMessage(ChatColor.GOLD + "Welcome new player!");
        	p.sendMessage(ChatColor.GOLD + "For a list of commands right click your backpack");
        	p.sendMessage(ChatColor.GOLD + "Left click 'Settings' and left click commands");
        	
            inv.setItem(8,compass);
            ItemMeta im = p.getInventory().getItem(8).getItemMeta();
            im.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&lBackpack"));
            List<String> lore2 = new ArrayList<String>();
            lore2.add(ChatColor.translateAlternateColorCodes('&', "&5Right Click to Open"));
            im.setLore(lore2);//set the lore
            p.getInventory().getItem(8).setItemMeta(im);
            
            p.setMaxHealth((double)400);
        }

        else{
           if(!p.getInventory().contains(compass)){
              inv.setItem(8,compass);
              ItemMeta iM = p.getInventory().getItem(8).getItemMeta();
              iM.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&lBackpack"));
              List<String> lore = new ArrayList<String>();
              lore.add(ChatColor.translateAlternateColorCodes('&', "&5Right Click to Open"));
              iM.setLore(lore);
              p.getInventory().getItem(8).setItemMeta(iM);
           }
        }

    }
	
	@EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerMoveCompass(InventoryDragEvent e){

		for(Integer x : e.getInventorySlots()) {
    	  if(x.intValue()==8) { 
             	e.setCancelled(true);
             	return;
         	}
         }
      }
	
	@EventHandler
	public void PlayerStatsCreate(final PlayerJoinEvent e) {
		File userfiles = new File("plugins" + File.separator + "Wizard101" + File.separator + "users" + File.separator + e.getPlayer().getName() + ".yml");
		if(!userfiles.exists()) {
			YamlConfiguration config = YamlConfiguration.loadConfiguration(userfiles);
			config.set("Name", e.getPlayer().getName());
			config.set("rank", "DEFAULT");
			config.set("level", Integer.valueOf(e.getPlayer().getLevel()));
			config.set("Health", Integer.valueOf(650));
			config.set("Mana", Integer.valueOf(26));
			config.set("crowns", Integer.valueOf(75));
			config.set("gold", Integer.valueOf(0));
			config.set("arena_tickets", Integer.valueOf(0));
			config.set("wizard_class", null);
			try {
				config.save(userfiles);
				Bukkit.getLogger().info(ChatColor.AQUA + "[Wizard101]" + " " + ChatColor.RED + "user file created for " + e.getPlayer().getName());
				e.getPlayer().sendMessage("Created your userfile!");
			} catch (IOException e2) {
				e2.printStackTrace();
			}
		}
	}
	
}