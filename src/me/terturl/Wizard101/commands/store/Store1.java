package me.terturl.Wizard101.commands.store;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Store1 implements Listener, CommandExecutor {

	public static Inventory MainShopLevel1;
	public static Inventory HelmetLevel1Shop;
	
	public Inventory MainLevel1Shop(Player p) {
		
		ItemStack PlaceHolder = new ItemStack(Material.PISTON_EXTENSION, 1);
		ItemMeta PH = PlaceHolder.getItemMeta();
		PH.setDisplayName("�aSpace");
		PlaceHolder.setItemMeta(PH);
		
		ItemStack HelmetBuyLevel1 = new ItemStack(Material.LEATHER_HELMET, 1);
		ItemMeta HBL1 = HelmetBuyLevel1.getItemMeta();
		HBL1.setDisplayName("�2Buy Helmets");
		HelmetBuyLevel1.setItemMeta(HBL1);
		
		ItemStack BootsBuyLevel1 = new ItemStack(Material.LEATHER_BOOTS, 1);
		ItemMeta BBL1 = BootsBuyLevel1.getItemMeta();
		BBL1.setDisplayName("�3Buy Boots");
		BootsBuyLevel1.setItemMeta(BBL1);
		
		ItemStack LeggingsBuyLevel1 = new ItemStack(Material.LEATHER_LEGGINGS, 1);
		ItemMeta LBL1 = LeggingsBuyLevel1.getItemMeta();
		LBL1.setDisplayName("�5Buy Leggings");
		LeggingsBuyLevel1.setItemMeta(LBL1);
		
		ItemStack RobeBuyLevel1 = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
		ItemMeta RBL1 = RobeBuyLevel1.getItemMeta();
		RBL1.setDisplayName("�6Buy Robes");
		RobeBuyLevel1.setItemMeta(RBL1);
		
		ItemStack RingBuyLevel1 = new ItemStack(Material.RECORD_3);
		ItemMeta RBL12 = RingBuyLevel1.getItemMeta();
		RBL12.setDisplayName("�7Buy Rings");
		RingBuyLevel1.setItemMeta(RBL12);
		
		ItemStack NecklaceLevel1 = new ItemStack(Material.RECORD_4);
		ItemMeta NL1 = NecklaceLevel1.getItemMeta();
		NL1.setDisplayName("�8Buy Necklaces");
		NecklaceLevel1.setItemMeta(NL1);
		
		ItemStack PetShop = new ItemStack(Material.LEASH);
		ItemMeta PS = PetShop.getItemMeta();
		PS.setDisplayName("�9Buy Pets");
		PetShop.setItemMeta(PS);
		
		MainShopLevel1 = Bukkit.createInventory(null, 9, "�bLevel 1 Shop");
		
		MainShopLevel1.setItem(0, PlaceHolder);
		MainShopLevel1.setItem(1, HelmetBuyLevel1);
		MainShopLevel1.setItem(2, RobeBuyLevel1);
		MainShopLevel1.setItem(3, LeggingsBuyLevel1);
		MainShopLevel1.setItem(4, BootsBuyLevel1);
		MainShopLevel1.setItem(5, RingBuyLevel1);
		MainShopLevel1.setItem(6, NecklaceLevel1);
		MainShopLevel1.setItem(7, PetShop);
		MainShopLevel1.setItem(8, PlaceHolder);
		
		return MainShopLevel1;
	}
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		
		Player p = (Player)cs;
		
		p.openInventory(MainLevel1Shop(p));
		
		return false;
	}
	
	@EventHandler
	 public void onClick(InventoryClickEvent e) {
		if (e.getInventory().getName().equalsIgnoreCase(MainShopLevel1.getName())) {
			   if (e.getCurrentItem() == null)
			    return;
			   if (e.getCurrentItem().getType() == Material.BEACON
					   	 && e.getCurrentItem().getItemMeta().hasDisplayName()
			     		 && e.getCurrentItem().getItemMeta().getDisplayName()
			     			.contains("�aSpace")) {
				   		e.setCancelled(true);
			   }
		   if (e.getCurrentItem().getType() == Material.LEATHER_HELMET
				     && e.getCurrentItem().getItemMeta().hasDisplayName()
				     && e.getCurrentItem().getItemMeta().getDisplayName()
				       .contains("�2Buy Helmets")) {
				    e.setCancelled(true);
				    ((Player) e.getWhoClicked()).chat("/level1");
				    e.getWhoClicked().closeInventory();
				   }
		   if (e.getCurrentItem().getType() == Material.LEATHER_CHESTPLATE
				     && e.getCurrentItem().getItemMeta().hasDisplayName()
				     && e.getCurrentItem().getItemMeta().getDisplayName()
				       .contains("�6Buy Robes")) {
				    e.setCancelled(true);
				    ((Player) e.getWhoClicked()).chat("/level1");
				    e.getWhoClicked().closeInventory();
				   }
		   if (e.getCurrentItem().getType() == Material.LEATHER_BOOTS
				     && e.getCurrentItem().getItemMeta().hasDisplayName()
				     && e.getCurrentItem().getItemMeta().getDisplayName()
				       .contains("�3Buy Boots")) {
				    e.setCancelled(true);
				    ((Player) e.getWhoClicked()).chat("/level1");
				    e.getWhoClicked().closeInventory();
				   }
		   if (e.getCurrentItem().getType() == Material.LEATHER_LEGGINGS
				     && e.getCurrentItem().getItemMeta().hasDisplayName()
				     && e.getCurrentItem().getItemMeta().getDisplayName()
				       .contains("�5Buy Leggings")) {
				    e.setCancelled(true);
				    ((Player) e.getWhoClicked()).chat("/level1");
				    e.getWhoClicked().closeInventory();
				   }
		   if (e.getCurrentItem().getType() == Material.RECORD_3
				     && e.getCurrentItem().getItemMeta().hasDisplayName()
				     && e.getCurrentItem().getItemMeta().getDisplayName()
				       .contains("�7Buy Rings")) {
				    e.setCancelled(true);
				    ((Player) e.getWhoClicked()).chat("/level1");
				    e.getWhoClicked().closeInventory();
				   }
		   if (e.getCurrentItem().getType() == Material.RECORD_4
				     && e.getCurrentItem().getItemMeta().hasDisplayName()
				     && e.getCurrentItem().getItemMeta().getDisplayName()
				       .contains("�8Buy Necklaces")) {
				    e.setCancelled(true);
				    ((Player) e.getWhoClicked()).chat("/level1");
				    e.getWhoClicked().closeInventory();
				   }
		   if (e.getCurrentItem().getType() == Material.LEASH
				     && e.getCurrentItem().getItemMeta().hasDisplayName()
				     && e.getCurrentItem().getItemMeta().getDisplayName()
				       .contains("�9Buy Pets")) {
				    e.setCancelled(true);
				    ((Player) e.getWhoClicked()).chat("/level1");
				    e.getWhoClicked().closeInventory();
				   }
		}
		
	}
	
}