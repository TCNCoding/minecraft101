package me.terturl.Wizard101.commands.store;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Store5 implements Listener, CommandExecutor {

	public static Inventory MainShopLevel5;
	public static Inventory HelmetLevel5Shop;
	
	public Inventory MainLevel5Shop(Player p) {
		
		ItemStack PlaceHolder = new ItemStack(Material.PISTON_EXTENSION, 1);
		ItemMeta PH = PlaceHolder.getItemMeta();
		PH.setDisplayName("�aSpace");
		PlaceHolder.setItemMeta(PH);
		
		ItemStack HelmetBuyLevel5 = new ItemStack(Material.LEATHER_HELMET, 1);
		ItemMeta HBL1 = HelmetBuyLevel5.getItemMeta();
		HBL1.setDisplayName("�2Buy Helmets 5");
		HelmetBuyLevel5.setItemMeta(HBL1);
		
		ItemStack BootsBuyLevel5 = new ItemStack(Material.LEATHER_BOOTS, 1);
		ItemMeta BBL1 = BootsBuyLevel5.getItemMeta();
		BBL1.setDisplayName("�3Buy Boots 5");
		BootsBuyLevel5.setItemMeta(BBL1);
		
		ItemStack LeggingsBuyLevel5 = new ItemStack(Material.LEATHER_LEGGINGS, 1);
		ItemMeta LBL1 = LeggingsBuyLevel5.getItemMeta();
		LBL1.setDisplayName("�5Buy Leggings 5");
		LeggingsBuyLevel5.setItemMeta(LBL1);
		
		ItemStack RobeBuyLevel5 = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
		ItemMeta RBL1 = RobeBuyLevel5.getItemMeta();
		RBL1.setDisplayName("�6Buy Robes 5");
		RobeBuyLevel5.setItemMeta(RBL1);
		
		ItemStack RingBuyLevel5 = new ItemStack(Material.RECORD_3);
		ItemMeta RBL12 = RingBuyLevel5.getItemMeta();
		RBL12.setDisplayName("�7Buy Rings 5");
		RingBuyLevel5.setItemMeta(RBL12);
		
		ItemStack NecklaceLevel5 = new ItemStack(Material.RECORD_4);
		ItemMeta NL1 = NecklaceLevel5.getItemMeta();
		NL1.setDisplayName("�8Buy Necklaces 5");
		NecklaceLevel5.setItemMeta(NL1);
		
		ItemStack PetShop = new ItemStack(Material.LEASH);
		ItemMeta PS = PetShop.getItemMeta();
		PS.setDisplayName("�9Buy Pets 5");
		PetShop.setItemMeta(PS);
		
		MainShopLevel5 = Bukkit.createInventory(null, 9, "�bLevel 5 Shop");
		
		MainShopLevel5.setItem(0, PlaceHolder);
		MainShopLevel5.setItem(1, HelmetBuyLevel5);
		MainShopLevel5.setItem(2, RobeBuyLevel5);
		MainShopLevel5.setItem(3, LeggingsBuyLevel5);
		MainShopLevel5.setItem(4, BootsBuyLevel5);
		MainShopLevel5.setItem(5, RingBuyLevel5);
		MainShopLevel5.setItem(6, NecklaceLevel5);
		MainShopLevel5.setItem(7, PetShop);
		MainShopLevel5.setItem(8, PlaceHolder);
		
		return MainShopLevel5;
	}
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		
		Player p = (Player)cs;
		
		p.openInventory(MainLevel5Shop(p));
		
		return false;
	}
	
	@EventHandler
	 public void onClick(InventoryClickEvent e) {
		if (e.getInventory().getName().equalsIgnoreCase(MainShopLevel5.getName())) {
			   if (e.getCurrentItem() == null)
			    return;
		if (e.getCurrentItem().getType() == Material.PISTON_EXTENSION
			     && e.getCurrentItem().getItemMeta().hasDisplayName()
			     && e.getCurrentItem().getItemMeta().getDisplayName()
			       .equals("�aSpace")) {
			    e.setCancelled(true);
			   }
		   if (e.getCurrentItem().getType() == Material.LEATHER_HELMET
				     && e.getCurrentItem().getItemMeta().hasDisplayName()
				     && e.getCurrentItem().getItemMeta().getDisplayName()
				       .contains("�2Buy Helmets 5")) {
				    e.setCancelled(true);
				    ((Player) e.getWhoClicked()).chat("/level5");
				    e.getWhoClicked().closeInventory();
				   }
		   if (e.getCurrentItem().getType() == Material.LEATHER_CHESTPLATE
				     && e.getCurrentItem().getItemMeta().hasDisplayName()
				     && e.getCurrentItem().getItemMeta().getDisplayName()
				       .contains("�6Buy Robes 5")) {
				    e.setCancelled(true);
				    ((Player) e.getWhoClicked()).chat("/level5");
				    e.getWhoClicked().closeInventory();
				   }
		   if (e.getCurrentItem().getType() == Material.LEATHER_BOOTS
				     && e.getCurrentItem().getItemMeta().hasDisplayName()
				     && e.getCurrentItem().getItemMeta().getDisplayName()
				       .contains("�3Buy Boots 5")) {
				    e.setCancelled(true);
				    ((Player) e.getWhoClicked()).chat("/level5");
				    e.getWhoClicked().closeInventory();
				   }
		   if (e.getCurrentItem().getType() == Material.LEATHER_LEGGINGS
				     && e.getCurrentItem().getItemMeta().hasDisplayName()
				     && e.getCurrentItem().getItemMeta().getDisplayName()
				       .contains("�5Buy Leggings 5")) {
				    e.setCancelled(true);
				    ((Player) e.getWhoClicked()).chat("/level5");
				    e.getWhoClicked().closeInventory();
				   }
		   if (e.getCurrentItem().getType() == Material.RECORD_3
				     && e.getCurrentItem().getItemMeta().hasDisplayName()
				     && e.getCurrentItem().getItemMeta().getDisplayName()
				       .contains("�7Buy Rings 5")) {
				    e.setCancelled(true);
				    ((Player) e.getWhoClicked()).chat("/level5");
				    e.getWhoClicked().closeInventory();
				   }
		   if (e.getCurrentItem().getType() == Material.RECORD_4
				     && e.getCurrentItem().getItemMeta().hasDisplayName()
				     && e.getCurrentItem().getItemMeta().getDisplayName()
				       .contains("�8Buy Necklaces 5")) {
				    e.setCancelled(true);
				    ((Player) e.getWhoClicked()).chat("/level5");
				    e.getWhoClicked().closeInventory();
				   }
		   if (e.getCurrentItem().getType() == Material.LEASH
				     && e.getCurrentItem().getItemMeta().hasDisplayName()
				     && e.getCurrentItem().getItemMeta().getDisplayName()
				       .contains("�9Buy Pets 5")) {
				    e.setCancelled(true);
				    ((Player) e.getWhoClicked()).chat("/level5");
				    e.getWhoClicked().closeInventory();
				   }
		}
		
	}
	
}