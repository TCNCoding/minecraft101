package me.terturl.Wizard101.commands;

import java.io.File;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class PlayerInfo implements CommandExecutor {

	static FileConfiguration config = null;
	
	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2,
			String[] arg3) {
		
		Player p = (Player)arg0;
		
		getPlayerInfo(p);
		
		return false;
	}
	
	public void getPlayerInfo(Player p) {
		File pf = new File("plugins" + File.separator + "Wizard101" + File.separator + "users" + File.separator + p.getName() + ".yml");
	    config = YamlConfiguration.loadConfiguration(pf);
	    p.sendMessage("Wizard Level: " + (Integer) config.get("level"));
	    p.sendMessage("You rank is: " + (String) config.get("rank"));
	    p.sendMessage("You have: " + (Integer) config.get("crowns") + " crowns");
	    p.sendMessage("You have: " + (Integer) config.get("gold") + " gold pieces");
	    p.sendMessage("You have: " + (Integer) config.get("arena_tickets") + " arena tickets");
	}
	
}