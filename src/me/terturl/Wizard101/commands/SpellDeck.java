package me.terturl.Wizard101.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class SpellDeck implements CommandExecutor
{
	public static Inventory spellDeckInv;
	public static FileConfiguration config = null;
	
	public Inventory spellDeck(Player p)
	{
		spellDeckInv = Bukkit.createInventory(null, 18, "�bSpell Deck");
		spellDeckInv.clear();
		return spellDeckInv;
	}
	
	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3)
	{
		if(arg0 instanceof Player)
		{
			Player p = (Player)arg0;
			p.openInventory(spellDeck(p));
		}
		return false;
	}
}
