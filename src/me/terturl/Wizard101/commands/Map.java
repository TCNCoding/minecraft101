package me.terturl.Wizard101.commands;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Map implements CommandExecutor
{
	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3)
	{
		if(arg0 instanceof Player)
		{
			Player p = (Player)arg0;
			p.getInventory().addItem(new ItemStack(Material.MAP, 1));
		}
		return false;
	}
}
