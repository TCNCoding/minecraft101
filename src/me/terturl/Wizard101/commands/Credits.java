package me.terturl.Wizard101.commands;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Credits implements CommandExecutor, Listener
{
	public static Inventory creditsInv;
	
	public Inventory showCredits(Player p)
	{
		ItemStack terturlWool = new ItemStack(Material.WOOL, 1, (short) 5);
		ItemStack minerfilWool = new ItemStack(Material.WOOL, 1, (short) 4);
		ItemStack sndWool = new ItemStack(Material.WOOL, 1, (short) 4);
		ItemStack mineturtleWool = new ItemStack(Material.WOOL, 1, (short) 14);
		ItemStack demonfireWool = new ItemStack(Material.WOOL, 1, (short) 14);
		ItemStack terminator931 = new ItemStack(Material.WOOL, 1, (short) 10);
		
		ItemMeta terturlMeta = terturlWool.getItemMeta();
		terturlMeta.setDisplayName("�aterturl");
		terturlMeta.setLore(Arrays.asList("�7Lead Developer of this Project"));
		terturlWool.setItemMeta(terturlMeta);
		
		ItemMeta minerfilMeta = minerfilWool.getItemMeta();
		minerfilMeta.setDisplayName("�eMiner_Fil");
		minerfilMeta.setLore(Arrays.asList("�7Developer of this Project"));
		minerfilWool.setItemMeta(minerfilMeta);
		
		ItemMeta sndMeta = sndWool.getItemMeta();
		sndMeta.setDisplayName("�exSnDxPROx");
		sndMeta.setLore(Arrays.asList("�7Tester/Leader of this Project"));
		sndWool.setItemMeta(sndMeta);
		
		ItemMeta mineturtleMeta = mineturtleWool.getItemMeta();
		mineturtleMeta.setDisplayName("�c_MineTurtle_");
		mineturtleMeta.setLore(Arrays.asList("�7Tester of this Project"));
		mineturtleWool.setItemMeta(mineturtleMeta);
		
		ItemMeta demonfireMeta = demonfireWool.getItemMeta();
		demonfireMeta.setDisplayName("�cDemonfire812");
		demonfireMeta.setLore(Arrays.asList("�7Tester of this Project"));
		demonfireWool.setItemMeta(demonfireMeta);
		
		ItemMeta terminator931Meta = terminator931.getItemMeta();
		terminator931Meta.setDisplayName("�9Helper");
		terminator931Meta.setLore(Arrays.asList("�7Helped with some building"));
		terminator931.setItemMeta(terminator931Meta);
		
		creditsInv = Bukkit.createInventory(null, 9, "�bCredits");
		creditsInv.clear();
		creditsInv.setItem(0, terturlWool);
		creditsInv.setItem(1, minerfilWool);
		creditsInv.setItem(2, sndWool);
		creditsInv.setItem(3, mineturtleWool);
		creditsInv.setItem(4, demonfireWool);
		creditsInv.setItem(5, terminator931);
		return creditsInv;
	}
	
	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3)
	{
		if(arg0 instanceof Player)
		{
			Player p = (Player)arg0;
			p.openInventory(showCredits(p));
		}
		return false;
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e)
	{
		if(e.getCurrentItem().getType() == Material.WOOL && e.getCurrentItem().getItemMeta().hasDisplayName())
		{
			e.setCancelled(true);
		}
	}
}
