package me.terturl.Wizard101.commands;

import java.io.IOException;

import me.terturl.Wizard101.util.PlayerStats;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandGiveCrowns implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label,
			String[] args) {
		
		if(cs.hasPermission("wizard.GM")) {
			if((args.length > 2) || (args.length < 1)) {
				cs.sendMessage("Usage: /givec <name> <amount>");
			}
			else if(Bukkit.getPlayer(args[0]) == null) {
				cs.sendMessage("Player not found");
			}
			else {
				int g = 0;
				try {
					g = Integer.parseInt(args[1]);
				} catch (NumberFormatException e) {
					cs.sendMessage(args[1] + "is not a valid number");
				}
				try {
					PlayerStats.setPlayerCrowns(args[0], g, 1);
					cs.sendMessage("gave " + g + " to " + args[0]);
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}
		} else {
			cs.sendMessage("You need GameMaster or up!");
		}
		
		return false;
	}
	
}