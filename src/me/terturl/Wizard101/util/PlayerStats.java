package me.terturl.Wizard101.util;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class PlayerStats {
	
	static FileConfiguration config = null;
	
	public static int getPlayerCrowns(String p) {
		File pf = new File("plugins" + File.separator + "Wizard101" + File.separator + "users" + File.separator + p + ".yml");
	    config = YamlConfiguration.loadConfiguration(pf);
	    return config.getInt("crowns");
	}
	
	public static void setPlayerCrowns(String p, int g, int r) throws IOException {
		File pf = new File("plugins" + File.separator + "Wizard101" + File.separator + "users" + File.separator + p + ".yml");
	    config = YamlConfiguration.loadConfiguration(pf);
	    config.set("crowns", Integer.valueOf(((Integer)config.get("crowns")).intValue() + g * r));
	    config.save(pf);
	}
	
	public static int getPlayerGold(String p) {
		File pf = new File("plugins" + File.separator + "Wizard101" + File.separator + "users" + File.separator + p + ".yml");
	    config = YamlConfiguration.loadConfiguration(pf);
	    return config.getInt("gold");
	}
	
	public static void setPlayerGold(String p, int g, int r) throws IOException {
		File pf = new File("plugins" + File.separator + "Wizard101" + File.separator + "users" + File.separator + p + ".yml");
	    config = YamlConfiguration.loadConfiguration(pf);
	    config.set("gold", Integer.valueOf(((Integer)config.get("gold")).intValue() + g * r));
	    config.save(pf);
	}
	
	public static int getPlayerArenaTickets(String p) {
		File pf = new File("plugins" + File.separator + "Wizard101" + File.separator + "users" + File.separator + p + ".yml");
	    config = YamlConfiguration.loadConfiguration(pf);
	    return config.getInt("arena_tickets");
	}
	
	public static void setPlayerArenaTickets(String p, int g, int r) throws IOException {
		File pf = new File("plugins" + File.separator + "Wizard101" + File.separator + "users" + File.separator + p + ".yml");
	    config = YamlConfiguration.loadConfiguration(pf);
	    config.set("arena_tickets", Integer.valueOf(((Integer)config.get("arena_tickets")).intValue() + g * r));
	    config.save(pf);
	}
	
	public static int getPlayerRank(String p) {
		File pf = new File("plugins" + File.separator + "Wizard101" + File.separator + "users" + File.separator + p + ".yml");
	    config = YamlConfiguration.loadConfiguration(pf);
	    if(config.getString("rank").equals("DEFAULT")) {
	    	return 1;
	    }
	    return 2;
	}
	
	public static void setPlayerRank(String p, int r) throws IOException {
		File pf = new File("plugins" + File.separator + "Wizard101" + File.separator + "users" + File.separator + p + ".yml");
	    config = YamlConfiguration.loadConfiguration(pf);
	    switch (r) {
	    case 1:
	    	config.set("rank", "DEFAULT");
	    	break;
	    case 2:
	    	config.set("rank", "PAIDUSER");
	    }
	}
	
}