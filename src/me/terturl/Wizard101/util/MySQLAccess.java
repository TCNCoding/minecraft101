package me.terturl.Wizard101.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import me.terturl.Wizard101.Main;

import org.bukkit.ChatColor;

public class MySQLAccess
{
	public static Connection connect = null;
	private static Statement statement = null;
    public Main plugin;

    public MySQLAccess(Main instance)
    {
        plugin = instance;
    }
	
	public static boolean openConnection(String par1Str)
	{
		Main.sendConsoleMessage(ChatColor.GREEN + "Opening MySQL Connection...");
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager.getConnection(par1Str);
            Main.sendConsoleMessage(ChatColor.GREEN + "MySQL Opened!");
			return true;
		} catch(Exception e) { Main.sendConsoleMessage(ChatColor.RED + "MySQL Connection Failed...");e.printStackTrace(); }
		return false;
	}
	
	public static ResultSet sendStatement(String par1Str)
	{
		try {
			statement = connect.createStatement();
			if(par1Str.startsWith("SELECT"))
				return statement.executeQuery(par1Str);
			else
				statement.executeUpdate(par1Str);
		} catch(Exception e) { e.printStackTrace(); }
		return null;
	}
	
	public static void closeConnection()
	{
        Main.sendConsoleMessage(ChatColor.GREEN + "Closing MySQL Connection...");
		try {
			if (statement != null)
				statement.close();
			if (connect != null)
				connect.close();
		} catch (Exception e) { Main.sendConsoleMessage(ChatColor.RED + "MySQL Closing Failed!"); e.printStackTrace(); }
        Main.sendConsoleMessage(ChatColor.GREEN + "MySQL Closed!");
	}
}